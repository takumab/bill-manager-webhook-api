import express from 'express';
import controller from '../controllers/getBillsController';

const router = express.Router();

router.post('/providers', controller.getBillsController);

export = router;
