import { Request, Response, NextFunction } from 'express';
import logging from '../config/logging';
import { ProviderHttpClient } from '../../billManager/adapters/providerHttpClient';
import { BillManager } from '../../billManager/billManager';
import { HttpClient } from '../../billManager/adapters/common/httpClient';

const NAMESPACE = 'Get Bills Controller';

const getBillsController = async (req: Request, res: Response, next: NextFunction): Promise<Response> => {
    logging.info(NAMESPACE, `Get bills route called.`);
    const providerHttpClient = new ProviderHttpClient();
    const http = new HttpClient();
    const billManager = new BillManager(providerHttpClient);
    try {
        const response = await billManager.getBillsFor(req.body.provider);
        await http.post(req.body.callbackUrl, response);
        return res.status(200).json();
    } catch (error) {
        logging.error(NAMESPACE, error);
        throw new Error(`New error ${error}`);
    }
};

export default { getBillsController };
