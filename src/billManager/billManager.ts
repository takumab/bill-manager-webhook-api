import { BillProviderHttpClient } from './ports/billProviderHttpClient';
import { Bill } from './models/bill';

export class BillManager {
    private billProviderHttpClient: BillProviderHttpClient;

    constructor(billProviderHttpClient: BillProviderHttpClient) {
        this.billProviderHttpClient = billProviderHttpClient;
    }

    async getBillsFor(provider: string): Promise<Bill[]> {
        return await this.billProviderHttpClient.get(provider);
    }
}
