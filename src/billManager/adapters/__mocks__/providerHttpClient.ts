import { BillProviderHttpClient } from '../../ports/billProviderHttpClient';
import { Bill } from '../../models/bill';

export class ProviderHttpClient implements BillProviderHttpClient {
    async get(_provider: string): Promise<Bill[]> {
        return [
            {
                billedOn: '2020-04-07T15:03:14.257Z',
                amount: 22.27
            },
            {
                billedOn: '2020-05-07T15:03:14.257Z',
                amount: 30.0
            }
        ];
    }
}
