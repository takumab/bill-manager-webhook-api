import { Bill } from '../../models/bill';
import fetch from 'node-fetch';
import logging from '../../../app/config/logging';
const NAMESPACE = 'HTTP Client';

export class HttpClient {
    async post(url: string, data: Bill[]): Promise<string> {
        logging.info(NAMESPACE, `Inside http client post function`);
        const options = {
            method: 'POST',
            body: JSON.stringify(data),
            headers: { 'Content-Type': 'application/json' }
        };
        try {
            const response = await fetch(url, options);
            return response.statusText;
        } catch (e) {
            logging.error(NAMESPACE, `${e}`);
            throw new Error();
        }
    }
}
