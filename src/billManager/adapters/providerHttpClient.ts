import { BillProviderHttpClient } from '../ports/billProviderHttpClient';
import { Bill } from '../models/bill';
import fetch from 'node-fetch';
import logging from '../../app/config/logging';
const NAMESPACE = 'ProviderHttpClient';

export class ProviderHttpClient implements BillProviderHttpClient {
    async get(provider: string): Promise<Bill[]> {
        logging.info(NAMESPACE, 'Inside provider http client');
        const cache = new Map();
        try {
            if (cache.get('data')) {
                logging.info(NAMESPACE, `Cache hit!!! ${cache.get('data')}`);
                return cache.get('data');
            } else {
                const response = await fetch(`http://localhost:3000/providers/${provider}`);
                const data = await response.json();
                cache.set('data', data);
                return data;
            }
        } catch (error) {
            logging.error(NAMESPACE, `Error ${error}`);
            return error;
        }
    }
}
