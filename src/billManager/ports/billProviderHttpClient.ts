import { Bill } from '../models/bill';

export interface BillProviderHttpClient {
    get(provider: string): Promise<Bill[]>;
}
