export interface Bill {
    billedOn: string;
    amount: number;
}
