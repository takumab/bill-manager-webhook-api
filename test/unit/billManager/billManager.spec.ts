import { Bill } from '../../../src/billManager/models/bill';
import { BillManager } from '../../../src/billManager/billManager';
import { ProviderHttpClient } from '../../../src/billManager/adapters/__mocks__/providerHttpClient';

describe('Bill Manager Service', () => {
    beforeEach(() => jest.clearAllMocks());

    it('should return bills from a provider', async () => {
        const expectedBillData: Bill[] = [
            {
                billedOn: '2020-04-07T15:03:14.257Z',
                amount: 22.27
            },
            {
                billedOn: '2020-05-07T15:03:14.257Z',
                amount: 30
            }
        ];
        const GAS_PROVIDER = 'gas';
        const providerHttpClient = new ProviderHttpClient();
        const billManager = new BillManager(providerHttpClient);
        const mockedBillDataResponse = await billManager.getBillsFor(GAS_PROVIDER);
        expect(mockedBillDataResponse).toEqual(expectedBillData);
    });
});
