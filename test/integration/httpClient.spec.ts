import { Bill } from '../../src/billManager/models/bill';
import { HttpClient } from '../../src/billManager/adapters/common/httpClient';

describe('When Http Client', () => {
    it('should send data to call back url', () => {
        const billData: Bill[] = [
            {
                billedOn: '2020-04-07T15:03:14.257Z',
                amount: 22.27
            },
            {
                billedOn: '2020-05-07T15:03:14.257Z',
                amount: 30
            }
        ];
        const http = new HttpClient();
        expect(http.post('https://f069b941c47d.ngrok.io', billData)).toEqual({ message: 'data sent' });
    });
});
