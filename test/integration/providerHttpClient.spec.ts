import { ProviderHttpClient } from '../../src/billManager/adapters/providerHttpClient';
import { Bill } from '../../src/billManager/models/bill';

describe('When ProviderHttpClient', () => {
    it('should return bill from gas provider', async () => {
        const expectedGasBillData: Bill[] = [
            {
                billedOn: '2020-04-07T15:03:14.257Z',
                amount: 22.27
            },
            {
                billedOn: '2020-05-07T15:03:14.257Z',
                amount: 30
            }
        ];
        const providerHttpClient = new ProviderHttpClient();

        const gasBillData = await providerHttpClient.get('gas');

        expect(gasBillData).toEqual(expectedGasBillData);
    });

    it('should return bill from internet provider', async () => {
        const expectedInternetBillData: Bill[] = [
            {
                amount: 15.12,
                billedOn: '2020-02-07T15:03:14.257Z'
            },
            {
                amount: 15.12,
                billedOn: '2020-03-07T15:03:14.257Z'
            }
        ];
        const providerHttpClient = new ProviderHttpClient();
        const internetBillData = await providerHttpClient.get('internet');

        expect(internetBillData).toEqual(expectedInternetBillData);
    });
});
